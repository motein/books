#本文件为一系列命令的集合，包括系统软件更新，GNURADIO/HACKRF/GR-OSMOCOM/RTL-SDR的安装
#准备工作：确认网络连接正常，把本文件放在home文件夹里，把hackrf-master解压到/home,打开终端，sudo su,输入密码进root
#注意：cd /home/xd/hackrf-master那行改成你机器实际的路径，可以用右键查看属性，xd是我自己的用户名
#终端中输入 sh /本文件路径/grc.sh
#即可自动安装
#适用于UBUNTU14.04中文版，其他版本未经实验
#WECHAT：HKCSG365
apt-get update -y
apt-get upgrade -y
sudo apt-get -y install libboost1.54-all-dev git 
sudo apt-get -y install build-essential
sudo apt-get -y install cmake
sudo apt-get -y install git-core 
sudo apt-get -y install autoconf 
sudo apt-get -y install automake 
sudo apt-get -y install libtool 
sudo apt-get -y install g++ 
sudo apt-get -y install python-dev 
sudo apt-get -y install swig 
sudo apt-get -y install pkg-config 
sudo apt-get -y install libfftw3-dev 
sudo apt-get -y install libcppunit-dev 
sudo apt-get -y install libgsl0-dev 
sudo apt-get -y install libusb-dev 
sudo apt-get -y install sdcc 
sudo apt-get -y install libsdl1.2-dev 
sudo apt-get -y install python-wxgtk2.8 
sudo apt-get -y install python-numpy 
sudo apt-get -y install python-cheetah 
sudo apt-get -y install python-lxml 
sudo apt-get -y install doxygen 
sudo apt-get -y install python-qt4 
sudo apt-get -y install python-qwt5-qt4 
sudo apt-get -y install libxi-dev 
sudo apt-get -y install libqt4-opengl-dev 
sudo apt-get -y install libqwt5-qt4-dev 
sudo apt-get -y install libfontconfig1-dev 
sudo apt-get -y install libxrender-dev 
sudo apt-get -y install libusb-1.0

cd /home
git clone --recursive https://github.com/gnuradio/gnuradio.git
cd gnuradio
mkdir build 
cd build
cmake ../ 
make 
sudo make install
sudo ldconfig

cd /home/xd/hackrf-master
cd host 
mkdir build 
cd build 
cmake ../
make 
sudo make install 
sudo ldconfig

cd /home
git clone --progress git://git.osmocom.org/rtl-sdr  
cd rtl-sdr 
mkdir build 
cd build 
cmake ../ 
make
sudo make install 
sudo ldconfig 

cd /home
git clone --progress git://git.osmocom.org/gr-osmosdr
cd gr-osmosdr 
mkdir build 
cd build 
cmake ../ 
make 
sudo make install 
sudo ldconfig 


